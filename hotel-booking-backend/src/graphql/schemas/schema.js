const { userSchema, userResolver } = require('./user/user');
const { hotelSchema, hotelResolver } = require('./hotel/hotel');
const { mergeResolvers, mergeTypeDefs } = require('@graphql-tools/merge');
const { merge } = require('lodash');

const typeDefs = [userSchema, hotelSchema];
const resolvers = merge(userResolver, hotelResolver);

module.exports = { typeDefs, resolvers };
