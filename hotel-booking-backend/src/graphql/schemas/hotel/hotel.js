const { gql } = require('apollo-server-express');

const hotelSchema = gql`
  extend type Query {
    getHotel(_id: ID): Hotel
    getHotels: [Hotel]
  }
  extend type Mutation {
    createHotel(
      name: String
      rating: Int
      about: String
      img: String
      location: String
      guest: Int
      room: Int
      price: Int
      discountedPrice: Int
    ): Hotel
    updateHotel(
      _id: ID
      name: String
      rating: Int
      about: String
      img: String
      location: String
      guest: Int
      room: Int
      price: Int
      discountedPrice: Int
    ): Hotel
    deleteHotel(_id: ID): ID
  }

  type Hotel {
    _id: ID
    name: String
    rating: Int
    about: String
    img: String
    location: String
    guest: Int
    room: Int
    price: Int
    discountedPrice: Int
  }
`;

const hotelResolver = {
  Query: {
    getHotel: require('../../queries/hotel/getHotel'),
    getHotels: require('../../queries/hotel/getHotels'),
  },
  Mutation: {
    createHotel: require('../../mutations/hotel/createHotel'),
    updateHotel: require('../../mutations/hotel/updateHotel'),
    deleteHotel: require('../../mutations/hotel/deleteHotel'),
  },
};

module.exports = { hotelSchema, hotelResolver };
