const { gql } = require('apollo-server-express');

const userSchema = gql`
  type Query {
    getUsers: [User]
    getUser(_id: ID!): User
    getCurrentUser: CurrentUser
  }
  type Mutation {
    createUser(userName: String, email: String!, password: String!): User
    updateUser(
      _id: ID
      userName: String
      email: String
      password: String
      hotel: ID
    ): User
    deleteUser(_id: ID): ID
    login(email: String, password: String): String
  }
  type User {
    _id: ID
    userName: String
    email: String!
    password: String!
    hotel: [Hotel]
  }
  type CurrentUser {
    userId: ID
    isAuth: Boolean
  }
  type Hotel {
    _id: ID
    name: String
    rating: Int
    about: String
    img: String
    location: String
    guest: Int
    room: Int
    price: Int
    discountedPrice: Int
  }
`;

const userResolver = {
  Query: {
    getUsers: require('../../queries/user/getUsers'),
    getUser: require('../../queries/user/getUser'),
    getCurrentUser: require('../../queries/user/getCurrentUser'),
  },
  Mutation: {
    createUser: require('../../mutations/user/createUser'),
    updateUser: require('../../mutations/user/updateUser'),
    deleteUser: require('../../mutations/user/deleteUser'),
    login: require('../../mutations/user/login'),
  },
};

module.exports = { userSchema, userResolver };
