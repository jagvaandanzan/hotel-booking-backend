const { ApolloServer } = require('apollo-server-express');
const { typeDefs, resolvers } = require('../schemas/schema');
const jwt = require('jsonwebtoken');
const apolloServer = new ApolloServer({
  typeDefs: typeDefs,
  resolvers: resolvers,
  context: ({ req }) => {
    const token = req.headers.authorization || '';
    try {
      console.log(token);
      const decode = jwt.verify(token, process.env.SECRET);
      return { isAuth: true, userId: decode.userId };
    } catch (e) {
      return { isAuth: false };
    }
  },
});

module.exports = { apolloServer };
