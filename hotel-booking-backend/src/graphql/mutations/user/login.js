const { User } = require('../../../model/models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = async (root, args, context, info) => {
  const user = await User.findOne({ email: args.email });
  if (!user) {
    throw new Error('No user found ');
  }
  const isValid = await bcrypt.compare(args.password, user.password);
  if (!isValid) {
    throw new Error('Incorrect information ');
  }
  const token = await jwt.sign(
    {
      userId: user._id,
    },
    process.env.SECRET,
    { expiresIn: '1y' }
  );
  return token;
};
