const { User } = require('../../../model/models');
const bcrypt = require('bcrypt');
module.exports = async (root, args, context, info) => {
  bcrypt.hash(args.password, 10, async (err, hash) => {
    const user = await User({
      ...args,
      password: hash,
    }).save();
    return user;
  });
};
