const { User } = require('../../../model/models');

module.exports = async (root, args, context, info) => {
  await User.findByIdAndDelete(args._id);
  return args._id;
};
