const { User } = require('../../../model/models');

module.exports = async (root, args, context, info) => {
  const user = await User.findByIdAndUpdate(
    args._id,
    {
      $addToSet: { hotel: args.hotel },
    },
    { new: true }
  ).populate('hotel');
  return user;
};
