const { Hotel } = require('../../../model/models');

module.exports = async (root, args, context, info) => {
  const hotel = await Hotel.findByIdAndUpdate(args._id, args, { new: true });
  return hotel;
};
