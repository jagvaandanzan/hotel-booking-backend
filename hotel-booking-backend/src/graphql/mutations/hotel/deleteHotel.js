const { Hotel } = require('../../../model/models');

module.exports = async (root, args, context, info) => {
  await Hotel.findByIdAndDelete(args._id);
  return args._id;
};
