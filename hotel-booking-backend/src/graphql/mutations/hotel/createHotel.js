const { Hotel } = require('../../../model/models');

module.exports = async (root, args, context, info) => {
  const hotel = await Hotel(args).save();
  return hotel;
};
