const { Hotel } = require('../../../model/models');

module.exports = async function (root, args, context, info) {
  const hotel = await Hotel.findById(args._id);
  return hotel;
};
