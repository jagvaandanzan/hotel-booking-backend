const { Hotel } = require('../../../model/models');

module.exports = async (root, args, context, info) => {
  const hotel = Hotel.find(args);
  return hotel;
};
