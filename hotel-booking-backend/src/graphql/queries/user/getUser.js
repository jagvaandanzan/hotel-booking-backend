const { User } = require('../../../model/models');

module.exports = async function (root, args, context, info) {
  const user = await User.findById(args._id).populate('hotel');
  return user;
};
