const { User } = require('../../../model/models');

module.exports = async function (root, args, context, info) {
  const user = await User.find(args);
  return user;
};
