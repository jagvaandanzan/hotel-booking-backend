const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = {
  User: mongoose.model(
    'User',
    new Schema(
      {
        userName: {
          type: String,
        },
        email: {
          type: String,
          required: true,
          unique: true,
        },
        password: {
          type: String,
          required: true,
        },
        hotel: [{ type: Schema.Types.ObjectId, ref: 'Hotel' }],
      },
      { collection: 'hotelUsers' }
    )
  ),
  Hotel: mongoose.model(
    'Hotel',
    new Schema(
      {
        name: {
          type: String,
        },
        rating: { type: Number },
        location: {
          type: String,
        },
        about: { type: String },
        img: { type: String },
        guest: {
          type: Number,
        },
        room: {
          type: Number,
        },
        price: {
          type: Number,
        },
        discountedPrice: {
          type: Number,
        },
      },
      { collection: 'hotels' }
    )
  ),
};
